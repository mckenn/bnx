import ether from './helpers/ether'
import {advanceBlock} from './helpers/advanceToBlock'
import {increaseTimeTo, duration} from './helpers/increaseTime'
import latestTime from './helpers/latestTime'
import EVMThrow from './helpers/EVMThrow'

const BigNumber = web3.BigNumber;

const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should();

const BNXCrowdsale = artifacts.require('BNXCrowdsale');
const BNX = artifacts.require('BNX');
const TokenTimelock = artifacts.require('TokenTimelock');

contract('Crowdsale', function ([owner, wallet, bountyWallet, advisersWallet, teamWallet, SCOWallet, investor, someone]) {

  before(async function() {
    //Advance to the next block to correctly read time in the solidity "now" function interpreted by testrpc
    await advanceBlock()
  })

  let crowdsale, token, teamTokenTimelock, SCOTokenTimelock,
  preICOStartTime, ICOStartTime, ICOEndTime,
  afterEndTime, teamReleaseTime, SCOReleaseTime,
  ICO_RATE, PRE_ICO_RATE, TOKEN_PRE_ICO_CAP, TOKEN_ICO_CAP,
  BOUNTY_SUPPLY, ADVISERS_SUPPLY, TEAM_SUPPLY,
  SCO_SUPPLY

  beforeEach(async function () {
    preICOStartTime = latestTime() + duration.weeks(1);
    ICOStartTime = preICOStartTime + 300 // in 5 minutes
    ICOEndTime = preICOStartTime + 600 // in 10 minutes
    teamReleaseTime = ICOEndTime + 300
    SCOReleaseTime = ICOEndTime + 400
    afterEndTime = ICOEndTime + duration.seconds(1);

    crowdsale = await BNXCrowdsale.new(
      preICOStartTime,
      ICOStartTime,
      ICOEndTime,
      wallet,
      bountyWallet,
      advisersWallet,
      teamWallet,
      teamReleaseTime,
      SCOWallet,
      SCOReleaseTime);

    token = BNX.at(await crowdsale.token());

    //timeLocks
    teamTokenTimelock = TokenTimelock.at(await crowdsale.teamTokenTimelock());
    SCOTokenTimelock = TokenTimelock.at(await crowdsale.SCOTokenTimelock());

    // crowdsale constants
    TOKEN_PRE_ICO_CAP = await crowdsale.TOKEN_PRE_ICO_CAP();
    TOKEN_ICO_CAP = await crowdsale.TOKEN_ICO_CAP();
    PRE_ICO_RATE = await crowdsale.PRE_ICO_RATE();
    ICO_RATE = await crowdsale.ICO_RATE();
    BOUNTY_SUPPLY = await crowdsale.BOUNTY_SUPPLY();
    ADVISERS_SUPPLY = await crowdsale.ADVISERS_SUPPLY();
    TEAM_SUPPLY = await crowdsale.TEAM_SUPPLY();
    SCO_SUPPLY = await crowdsale.SCO_SUPPLY();
  });


  it('should create crowdsale with correct parameters', async function () {
    crowdsale.should.exist;
    token.should.exist;

    (await crowdsale.preICOStartTime()).should.be.bignumber.equal(preICOStartTime);
    (await crowdsale.ICOStartTime()).should.be.bignumber.equal(ICOStartTime);
    (await crowdsale.ICOEndTime()).should.be.bignumber.equal(ICOEndTime);
    (await crowdsale.PRE_ICO_RATE()).should.be.bignumber.equal(PRE_ICO_RATE);
    (await crowdsale.ICO_RATE()).should.be.bignumber.equal(ICO_RATE);
    (await crowdsale.wallet()).should.be.equal(wallet);
    (await crowdsale.bountyWallet()).should.be.equal(bountyWallet);
    (await crowdsale.advisersWallet()).should.be.equal(advisersWallet);
    (await crowdsale.TOKEN_PRE_ICO_CAP()).should.be.bignumber.equal(TOKEN_PRE_ICO_CAP);
    (await crowdsale.TOKEN_ICO_CAP()).should.be.bignumber.equal(TOKEN_ICO_CAP);
  });

  it('should set team timelock', async function () {
    // right before team timelock
    await teamTokenTimelock.release().should.be.rejected
    await increaseTimeTo(teamReleaseTime);
    await teamTokenTimelock.release().should.be.fulfilled;

    (await token.balanceOf(teamWallet)).should.be.bignumber.equal(TEAM_SUPPLY)
  });

  it('should set SCO timelock', async function () {
    // right before SCO timelock
    await SCOTokenTimelock.release().should.be.rejected
    await increaseTimeTo(SCOReleaseTime);
    await SCOTokenTimelock.release().should.be.fulfilled;

    (await token.balanceOf(SCOWallet)).should.be.bignumber.equal(SCO_SUPPLY)
  });

  it('should disable token transfers until ICO end', async function () {
    await increaseTimeTo(preICOStartTime);
    await crowdsale.buyTokens(investor, {value: 1, from: investor})
    await token.transfer(someone, 1, { from: investor }).should.be.rejected

    // allow transfers from bountyWallet and advisersWallet
    await token.transfer(someone, 1, { from: bountyWallet }).should.be.fulfilled
    await token.transfer(someone, 1, { from: advisersWallet }).should.be.fulfilled



    await increaseTimeTo(afterEndTime);
    await token.transfer(someone, 1, { from: investor }).should.be.fulfilled
  });

  it('should accept payments according to ICO schedule', async function () {
    // reject before start
    await crowdsale.send(ether(1)).should.be.rejected;
    await crowdsale.buyTokens(investor, {from: investor, value: ether(1)}).should.be.rejected
    // accept during ICO`
    await increaseTimeTo(preICOStartTime);
    await crowdsale.sendTransaction({value:1, from: investor}).should.be.fulfilled;
    // reject after ICO
    await increaseTimeTo(afterEndTime);
    await crowdsale.buyTokens(investor, {value: ether(1), from: investor}).should.be.rejected;
  });

  it('should apply correct rate', async function () {
    var balanceBefore, balanceAfter, weiAmount = ether(1);


      await increaseTimeTo(preICOStartTime);
      balanceBefore = await token.balanceOf(investor);
      await crowdsale.sendTransaction({value: weiAmount, from: investor}).should.be.fulfilled;
      balanceAfter = await token.balanceOf(investor);
      balanceAfter.minus(balanceBefore).should.be.bignumber.equal(weiAmount.mul(PRE_ICO_RATE))

      await increaseTimeTo(ICOStartTime);
      balanceBefore = await token.balanceOf(investor);
      await crowdsale.sendTransaction({value: weiAmount, from: investor}).should.be.fulfilled;
      balanceAfter = await token.balanceOf(investor);
      balanceAfter.minus(balanceBefore).should.be.bignumber.equal(weiAmount.mul(ICO_RATE))
  });


  it('should reject payments over TOKEN_PRE_ICO_CAP and TOKEN_ICO_CAP', async function () {
    await increaseTimeTo(preICOStartTime);
    const max_preICO_wei = TOKEN_PRE_ICO_CAP.div(PRE_ICO_RATE).floor()
    await crowdsale.send(max_preICO_wei).should.be.fulfilled;
    await crowdsale.send(1).should.be.rejected;

    await increaseTimeTo(ICOStartTime);
    const max_ico_wei = TOKEN_ICO_CAP.div(ICO_RATE).floor()
    await crowdsale.send(max_ico_wei).should.be.fulfilled;
    await crowdsale.send(1).should.be.rejected;
  });

  it('should allow bonus after ICO', async function () {
    await increaseTimeTo(preICOStartTime);
    await crowdsale.sendTransaction({from: investor, value: ether(1)});
    await crowdsale.sendTransaction({from: someone, value: ether(3)});
    await crowdsale.claimBonus({from: investor}).should.be.rejected;
    await crowdsale.claimBonusFor(investor).should.be.rejected;
    await increaseTimeTo(afterEndTime);
    await crowdsale.claimBonus({from: investor}).should.be.fulfilled;
    await crowdsale.claimBonus({from: investor}).should.be.rejected;
    await crowdsale.claimBonusFor(someone).should.be.fulfilled;
    await crowdsale.claimBonusFor(someone).should.be.rejected;

    var balance1 = await token.balanceOf(investor);
    var balance2 = await token.balanceOf(someone);
    var newCap = balance1.add(balance2);
    newCap.should.be.bignumber.equal(TOKEN_PRE_ICO_CAP.add(TOKEN_ICO_CAP))
  });
});
