pragma solidity ^0.4.11;


import "zeppelin-solidity/contracts/token/MintableToken.sol";
import "zeppelin-solidity/contracts/token/LimitedTransferToken.sol";

/**
* @title BNX
*/
contract BNX is MintableToken, LimitedTransferToken {

    string public constant name = "BNX";
    string public constant symbol = "BNX";
    uint8 public constant decimals = 18;

    uint256 ICOEndTime;
    address bountyWallet;
    address advisersWallet;

    function BNX(uint256 _ICOEndTime, address _bountyWallet, address _advisersWallet) {
        ICOEndTime = _ICOEndTime;
        bountyWallet = _bountyWallet;
        advisersWallet = _advisersWallet;
    }

    function transferableTokens(address holder, uint64 time) public constant returns (uint256) {
        // allow transfers after the end of ICO
        bool afterICO = time > ICOEndTime;
        bool advisersOrBountyWallet = holder == bountyWallet || holder == advisersWallet;
        return ( afterICO || advisersOrBountyWallet ) ? balanceOf(holder) : 0;
    }
}
