pragma solidity ^0.4.11;

import "zeppelin-solidity/contracts/token/TokenTimelock.sol";
import "zeppelin-solidity/contracts/ownership/Ownable.sol";
import "./BNX.sol";

/**
* @title BNXCrowdsale
*/
contract BNXCrowdsale is Ownable {
    using SafeMath for uint256;
    // The token being sold
    MintableToken public token;

    // start and end timestamps where investments are allowed (both inclusive)
    uint256 public preICOStartTime;
    uint256 public ICOStartTime;
    uint256 public ICOEndTime;

    // address where funds are collected
    address public wallet;

    address public bountyWallet;
    address public advisersWallet;

    address teamWallet;
    address SCOWallet;

    // how many token units a buyer gets per wei
    uint256 public PRE_ICO_RATE = 40000;
    uint256 public ICO_RATE = 20000;

    // amount of raised money in wei
    uint256 public weiRaised;

    uint256 public tokenSoldPreICO;
    uint256 public tokenSoldICO;
    uint256 public tokenSold;

    uint256 public constant TOKEN_PRE_ICO_CAP = 1000000000 * (10 ** uint256(18));
    uint256 public constant TOKEN_ICO_CAP = 3000000000 * (10 ** uint256(18)); // 45000000+62797500+588000000 BNX

    TokenTimelock public teamTokenTimelock;
    TokenTimelock public SCOTokenTimelock;

    uint256 public constant BOUNTY_SUPPLY = 500000000 * (10 ** uint256(18));
    uint256 public constant ADVISERS_SUPPLY = 500000000 * (10 ** uint256(18));

    uint256 public constant TEAM_SUPPLY = 2000000000 * (10 ** uint256(18));
    uint256 public constant SCO_SUPPLY = 3000000000 * (10 ** uint256(18));

    mapping (address => uint256) preICOPurchases;
    mapping (address => uint256) ICOPurchases;
    mapping (address => bool) bonusFilled;


    function BNXCrowdsale(
        uint256 _preICOStartTime,
        uint256 _ICOStartTime,
        uint256 _ICOEndTime,
        address _wallet,
        address _bountyWallet,
        address _advisersWallet,
        address teamWallet,
        uint64 teamReleaseTime,
        address SCOWallet,
        uint64 SCOReleaseTime
        ) {
            preICOStartTime = _preICOStartTime;
            ICOStartTime = _ICOStartTime;
            ICOEndTime = _ICOEndTime;

            require(preICOStartTime >= now);
            require(ICOStartTime >= preICOStartTime);
            require(ICOEndTime >= ICOStartTime);

            require(_wallet != 0x0);
            require(_bountyWallet != 0x0);
            require(_advisersWallet != 0x0);
            require(teamWallet != 0x0);
            require(SCOWallet != 0x0);

            wallet = _wallet;
            bountyWallet = _bountyWallet;
            advisersWallet = _advisersWallet;

            token = new BNX(ICOEndTime, bountyWallet, advisersWallet);

            teamTokenTimelock = new TokenTimelock(token, teamWallet, teamReleaseTime);
            token.mint(teamTokenTimelock, TEAM_SUPPLY);

            SCOTokenTimelock = new TokenTimelock(token, SCOWallet, SCOReleaseTime);
            token.mint(SCOTokenTimelock, SCO_SUPPLY);

            token.mint(bountyWallet, BOUNTY_SUPPLY);
            token.mint(advisersWallet, ADVISERS_SUPPLY);
        }

        /**
        * event for token purchase logging
        * @param purchaser who paid for the tokens
        * @param beneficiary who got the tokens
        * @param value weis paid for purchase
        * @param amount amount of tokens purchased
        */
        event TokenPurchase(address indexed purchaser, address indexed beneficiary, uint256 value, uint256 amount);

        // fallback function can be used to buy tokens
        function () payable {
            buyTokens(msg.sender);
        }

        // low level token purchase function
        function buyTokens(address beneficiary) public payable {
            require(msg.value != 0);

            uint256 weiAmount = msg.value;
            uint256 tokenAmount;

            if (now < ICOStartTime) {
              tokenAmount = weiAmount.mul(PRE_ICO_RATE);
              tokenSoldPreICO = tokenSoldPreICO.add(tokenAmount);
              preICOPurchases[beneficiary] = preICOPurchases[beneficiary].add(tokenAmount);
            } else {
              tokenAmount = weiAmount.mul(ICO_RATE);
              tokenSoldICO = tokenSoldICO.add(tokenAmount);
              ICOPurchases[beneficiary] = ICOPurchases[beneficiary].add(tokenAmount);
            }

            weiRaised = weiRaised.add(weiAmount);
            tokenSold = tokenSold.add(tokenAmount);

            require(validPurchase());

            token.mint(beneficiary, tokenAmount);
            TokenPurchase(msg.sender, beneficiary, weiAmount, tokenAmount);

            wallet.transfer(msg.value);
        }

        // @return true if investors can buy at the moment
        function validPurchase() internal constant returns (bool) {
            bool withinTokenPreICOCap = tokenSoldPreICO <= TOKEN_PRE_ICO_CAP;
            bool withinTokenICOCap = tokenSoldICO <= TOKEN_ICO_CAP;
            bool withinPeriod = now >= preICOStartTime && now <= ICOEndTime;
            return withinTokenPreICOCap && withinTokenICOCap && withinPeriod;
        }

        // @return true if crowdsale event has ended
        function hasEnded() public constant returns (bool) {
            bool capReached = tokenSoldICO >= TOKEN_ICO_CAP;
            return now > ICOEndTime || capReached;
        }

        function claimBonus() public {
          claimBonusFor(msg.sender);
        }

        function claimBonusFor(address beneficiary) public {
          require(hasEnded() && !bonusFilled[beneficiary]);
          uint256 currentPurchase = preICOPurchases[beneficiary] + ICOPurchases[beneficiary];
          uint256 targetPurchase = currentPurchase.mul(TOKEN_PRE_ICO_CAP + TOKEN_ICO_CAP).div(tokenSold);
          token.mint(beneficiary, targetPurchase - currentPurchase);
          bonusFilled[beneficiary] = true;
        }
    }
