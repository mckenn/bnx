const BNXCrowdsale = artifacts.require("BNXCrowdsale")


module.exports = function(deployer, network, [owner, wallet, bountyWallet, advisersWallet, teamWallet, SCOWallet]) {
  const BigNumber = web3.BigNumber
  const preICOStartTime = web3.eth.getBlock(web3.eth.blockNumber).timestamp + 1
  const ICOStartTime = preICOStartTime + 300 // in 5 minutes
  const ICOEndTime = preICOStartTime + 600 // in 10 minutes
  const teamReleaseTime = ICOEndTime + 300
  const SCOReleaseTime = ICOEndTime + 400

  deployer.deploy(
    BNXCrowdsale,
    preICOStartTime,
    ICOStartTime,
    ICOEndTime,
    wallet,
    bountyWallet,
    advisersWallet,
    teamWallet,
    teamReleaseTime,
    SCOWallet,
    SCOReleaseTime)
}
